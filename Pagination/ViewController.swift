//
//  ViewController.swift
//  Pagination
//
//  Created by Halisson da Silva Jesus on 27/11/18.
//  Copyright © 2018 halissonsj. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    var limit = 20
    let cellId = "cell"
    var paging: Bool = false
    var tableView: UITableView!
    var activity: UIActivityIndicatorView?
    var context = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        activity = UIActivityIndicatorView(style: .gray)
        tableView = UITableView(frame: view.frame, style: .grouped)
        setupTableViewLayout()
    }
    
    func setupTableViewLayout() {
        view.addSubview(tableView)
        tableView.register(PaginationTableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.tableFooterView = activity
        tableView.backgroundColor = UIColor.yellow
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.top)
            make.bottom.equalTo(view.snp.bottom)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
        }
    }
    
    func geraItensArray() {
        let newItems  = (self.context.count ... self.context.count + 20).map {index in index}
        self.context.append(contentsOf: newItems)
    }
    
    @objc func pagingTableView() {
        paging = true
        print("paging")
        animate(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.geraItensArray()
            self.paging = false
            self.animate(false)
            self.tableView.reloadData()
        }
    }
    
    func animate(_ on: Bool) {
        if on {
            self.activity?.isHidden = false
            activity?.startAnimating()
        } else {
            self.activity?.stopAnimating()
            self.activity?.isHidden = true
        }
    }
}
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return context.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as? PaginationTableViewCell
        
        cell?.textLabel?.text = "row \(context[indexPath.row])"
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("row \(indexPath.row) selected")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height {
            if !paging {
                pagingTableView()
            }
        }
    }
    
}

