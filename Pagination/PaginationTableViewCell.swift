//
//  PaginationTableViewCell.swift
//  Pagination
//
//  Created by Halisson da Silva Jesus on 27/11/18.
//  Copyright © 2018 halissonsj. All rights reserved.
//

import UIKit
import SnapKit

class PaginationTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(with text: String) {
        let label = UILabel()
        label.text = text
        self.addSubview(label)
        
        label.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.centerY.equalTo(self)
            make.left.equalTo(self).offset(20)
            make.right.equalTo(self).offset(-20)
        }
    }

}
